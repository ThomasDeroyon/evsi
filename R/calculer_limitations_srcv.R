
#' Calcul des nombres de personne concernées par des limitations dans les activités de la vie quotidienne
#'
#'
#' Les calculs sont effectués sur les données individuelles de SRCV. Les effectifs sont calculés par
#' sexe et classe d'âge quinquennal, l'âge considéré étant l'âge au moment de l'enquête.
#' Le champ de SCRV étant restreint aux 16 ans ou plus, pour les questions individuelles, les effectifs
#' ne sont pas calculables sur les premières classes d'âge.
#'
#' @param chemin adresse physique du fichier (sas) contenant les données individuelles de SRCV pour l'année
#'        concernée
#'
#' @return La fonction revoie une liste de six vecteurs de même longueur, contenant les nombres de personne
#'    déclarant des limitations fonctionnelles légères, fortes et ne déclarant pas de limitations fonctionnelles
#'    par sexe et âge quinquennal au moment de l'enquête. Tous les vecteurs sont nommés par la classe d'âge
#'    représentée par l'âge le plus faible de la classe.
#'    \itemize{
#'    \item stronglylimited_homme : vecteur nommé contenant les estimations dans SRCV du nombre d'hommes
#'    déclarant des limitations fortes par âge
#'    \item limited_homme : vecteur nommé contenant les estimations dans SRCV du nombre d'hommes
#'    déclarant des limitations modérées par âge
#'    \item notlimited_homme : vecteur nommé contenant les estimations dans SRCV du nombre d'hommes
#'    ne déclarant pas de limitations modérées par âge
#'    \item stronglylimited_femme : vecteur nommé contenant les estimations dans SRCV du nombre de femmes
#'    déclarant des limitations fortes par âge
#'    \item limited_femme : vecteur nommé contenant les estimations dans SRCV du nombre de femmes
#'    déclarant des limitations modérées par âge
#'    \item notlimited_femme : vecteur nommé contenant les estimations dans SRCV du nombre de femmes
#'    ne déclarant pas de limitations modérées par âge
#'    }
#'
#' @author Thomas Deroyon

calculer_limitations_srcv <-
  function(chemin){

    srcv <- haven::read_sas(chemin)
    names(srcv) <- tolower(names(srcv))

    srcv$age_q <- 5 * (srcv$age %/% 5)
    srcv$age_q[srcv$age_q %in% c(90, 95, 100, 105, 110)] <- 85

    if (exists('pb040', where = srcv)){

      gali <- srcv %>% filter(adultvrairep == '1') %>% group_by(sexe, dim, age_q) %>%
        summarise(effectif = sum(pb040), freq = n()) %>% rename(age = age_q)

      stronglylimited_homme <-
        setNames(gali$effectif[gali$sexe == '1' & gali$dim == '1'],
                 nm = gali$age[gali$sexe == '1' & gali$dim == '1'])
      limited_homme <-
        setNames(gali$effectif[gali$sexe == '1' & gali$dim == '2'],
                 nm = gali$age[gali$sexe == '1' & gali$dim == '2'])
      notlimited_homme <-
        setNames(gali$effectif[gali$sexe == '1' & gali$dim == '3'],
                 nm = gali$age[gali$sexe == '1' & gali$dim == '3'])

      stronglylimited_femme <-
        setNames(gali$effectif[gali$sexe == '2' & gali$dim == '1'],
                 nm = gali$age[gali$sexe == '2' & gali$dim == '1'])
      limited_femme <-
        setNames(gali$effectif[gali$sexe == '2' & gali$dim == '2'],
                 nm = gali$age[gali$sexe == '2' & gali$dim == '2'])
      notlimited_femme <-
        setNames(gali$effectif[gali$sexe == '2' & gali$dim == '3'],
                 nm = gali$age[gali$sexe == '2' & gali$dim == '3'])

      return(
        list(
          stronglylimited_homme =
            stronglylimited_homme[names(stronglylimited_homme)[order(as.numeric(names(stronglylimited_homme)))]],
          limited_homme =
            limited_homme[names(limited_homme)[order(as.numeric(names(limited_homme)))]],
          notlimited_homme =
            notlimited_homme[names(notlimited_homme)[order(as.numeric(names(notlimited_homme)))]],
          stronglylimited_femme =
            stronglylimited_femme[names(stronglylimited_femme)[order(as.numeric(names(stronglylimited_femme)))]],
          limited_femme =
            limited_femme[names(limited_femme)[order(as.numeric(names(limited_femme)))]],
          notlimited_femme =
            notlimited_femme[names(notlimited_femme)[order(as.numeric(names(notlimited_femme)))]]
        )
      )

    } else {

      return(NA)

    }

  }
