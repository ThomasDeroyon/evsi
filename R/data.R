
#' Population par âge en France entière au 1er janvier 2018 (publication de janvier 2019)
#'
#' Un data.frame contenant une ligne par âge. Tous les âges supérieurs ou égaux à 100 sont regroupés dans la même modalité
#'
#' @format 101 lignes et 5 colonnes
#'     \itemize{
#'     \item naissance : année de naissance
#'     \item age : âge
#'     \item homme : nombre d'hommes par âge
#'     \item femme : nombre de femmes par âge
#'     \item total : nombre de personnes par âge}
#'
"pop_1"

#' Population par âge en France entière au 1er janvier 2019 (publication de janvier 2019)
#'
#' Un data.frame contenant une ligne par âge. Tous les âges supérieurs ou égaux à 100 sont regroupés dans la même modalité
#'
#' @format 101 lignes et 5 colonnes
#'     \itemize{
#'     \item naissance : année de naissance
#'     \item age : âge
#'     \item homme : nombre d'hommes par âge
#'     \item femme : nombre de femmes par âge
#'     \item total : nombre de personnes par âge}
#'
"pop_2"

#' Nombre de décès par âge au cours de l'année 2018 en France entière (données transmises par l'Insee à l'été 2019)
#'
#' Un data.frame contenant une ligne par âge. Tous les âges supérieurs à 110 ans sont agrégés dans une seule catégorie.
#'
#' @format 111 lignes et 4 colonnes
#' \itemize{
#' \item age : âge de décès
#' \item homme : nombre d'hommes ayant décédé en 2018 par âge
#' \item femme : nombre de femmes ayant décédé en 2018 par âge
#' \item total : nombre total de personnes ayant décédé en 2018 par âge }
"deces"

#' Prévalence des incapacités, légères ou lourdes, des incapacités légères et des incapacités lourdes par sexe et âge
#'
#' Un data.frame contenant une ligne par croisement d'âge, de sexe et de type d'incapacité (légère, lourde, légère ou lourde)
#'
#' @format 96 lignes et 4 colonnes
#' \itemize{
#' \item age : âge
#' \item sexe : sexe
#' \item incapacité : niveau d'incapacité pour laquelle sont calculées les prévalences (l = légère, sl = lourdes,
#'     l + sl = légère ou lourde)
#' \item prevalence : valeur des prévalences }
#'
#'
"prevalences"

#' Nombre de personnes par niveau d'incapacité en 2018 et en France métropolitaine
#'
#' Un data.frame contenant l'estimation issue de SRCV par sexe et classe d'âge quinquennal du nombre de personnes de 15 ans ou plus
#'  habitant en ménage ordinaire en France métropolitaine suivant les limitations qu'elles rencontrent das leur vie quotidienne
#'
#' @format 30 lignes et 7 colonnes
#' \itemize{
#' \item sexe : sexe de la personne (H ou F)
#' \item age : tranche d'âge quinquennal, résumé par le premier âge de la tranche (15 : 15-19 ans par exemple)
#' \item stronglylimited : nombre de personnes déclarant en 2018 des limitations sévères
#' \item limited : nombre de personnes déclarant en 2018 des limitations non sévères
#' \item notlimited : nombre de personnes ne déclarant pas de limitations en 2018
#' \item missing : nombre de personnes pour lesquelles la réponse à la question sur les limitations est manquante
#' \item total}
"incapacites"

#' Espérances de vie par âge pour les hommes en 2018
#'
#' Un data.frame contenant les espérances de vie par âge de 0 à 100 ans pour les hommes en 2018 et l'ensemble des données et des
#' intermédiaires utilisés pour leur calcul
#'
#' @format 111 lignes et 12 colonnes
#' \itemize{
#' \item age : âge
#' \item pop_1 : population par âge au 1er janvier 2018
#' \item pop_2 : population par âge au 1er janvier 2019
#' \item pop : nombre d'années vécues au cours de l'année 2018 par les personnes d'un âge donné
#' \item deces : nombre de décès par âge au cours de l'année 2018
#' \item mx : taux de mortalité par âge en 2018
#' \item qx : probabilité de décès par âge en 2018
#' \item lx : nombre de membres de la cohorte fictive de 100 000 individus à la naissance par âge
#' \item dx : nombre de décès par âge dans la cohorte
#' \item Lx : nombre d'années vécues au cours entre deux âges par les membres de la cohorte fictive d'un âge donné
#' \item Tx : nombe cumulé d'années vécues par les membres de la cohorte fictive d'un âge donné
#' \item ex : espérance de vie à un âge donné}
"ev_h"

#' Espérances de vie par âge pour les femmes en 2018
#'
#' Un data.frame contenant les espérances de vie par âge de 0 à 100 ans pour les femmes en 2018 et l'ensemble des données et des
#' intermédiaires utilisés pour leur calcul
#'
#' @format 111 lignes et 12 colonnes
#' \itemize{
#' \item age : âge
#' \item pop_1 : population par âge au 1er janvier 2018
#' \item pop_2 : population par âge au 1er janvier 2019
#' \item pop : nombre d'années vécues au cours de l'année 2018 par les personnes d'un âge donné
#' \item deces : nombre de décès par âge au cours de l'année 2018
#' \item mx : taux de mortalité par âge en 2018
#' \item qx : probabilité de décès par âge en 2018
#' \item lx : nombre de membres de la cohorte fictive de 100 000 individus à la naissance par âge
#' \item dx : nombre de décès par âge dans la cohorte
#' \item Lx : nombre d'années vécues au cours entre deux âges par les membres de la cohorte fictive d'un âge donné
#' \item Tx : nombe cumulé d'années vécues par les membres de la cohorte fictive d'un âge donné
#' \item ex : espérance de vie à un âge donné}
"ev_f"

#' Espérances de vie sans incapacité pour les hommes en 2018
#'
#' Un data.frame contenant les evsi pour les hommes par âge quinquennal (les moins de 15 ans étant regroupés en une seule tranche)
#' avec les données nécessaires à leur calcul et les intermédiaires de calcul
#'
#' @format 16 lignes et 11 colonnes
#' \itemize{
#' \item age : âge
#' \item lx : taille de la cohorte fictive de 100 000 personnes à la naissance par âge
#' \item dx : décès par âge dans la cohorte fictive
#' \item Lx : nombre d'années vécues entre deux âges par les membres de la cohorte fictive d'un âge donné
#' \item ex : espérance de vie par âge
#' \item Px : prévalence par âge des limitations
#' \item Lxfree : nombre d'années sans limitations vécues entre deux âges par les membres de la cohorte d'un âge donné
#' \item Txfree : nombre total d'années sans limitations vécues par les membres de la cohorte d'un âge donné
#' \item exfree : espérance de vie sans incapacité par âge
#' \item propxfree : part de l'espérance de vie passée sans incapacité par âge}
"evsi_h"

#' Espérances de vie sans incapacité pour les femmes en 2018
#'
#' Un data.frame contenant les evsi pour les femmes par âge quinquennal (les moins de 15 ans étant regroupés en une seule tranche)
#' avec les données nécessaires à leur calcul et les intermédiaires de calcul
#'
#' @format 16 lignes et 11 colonnes
#' \itemize{
#' \item age : âge
#' \item lx : taille de la cohorte fictive de 100 000 personnes à la naissance par âge
#' \item dx : décès par âge dans la cohorte fictive
#' \item Lx : nombre d'années vécues entre deux âges par les membres de la cohorte fictive d'un âge donné
#' \item ex : espérance de vie par âge
#' \item Px : prévalence par âge des limitations
#' \item Lxfree : nombre d'années sans limitations vécues entre deux âges par les membres de la cohorte d'un âge donné
#' \item Txfree : nombre total d'années sans limitations vécues par les membres de la cohorte d'un âge donné
#' \item exfree : espérance de vie sans incapacité par âge
#' \item propxfree : part de l'espérance de vie passée sans incapacité par âge}
"evsi_f"

#' Espérances de vie sans incapacités légères et sans incapacités lourdes en 2018
#'
#' Un data.frame contenant les espérances de vie sans incapacités légères d'une part et sans incapacités lourdes
#' d'autres part pour 2018, par âge, et pour les hommes et les femmes, avec les données nécessaires à leur calcul
#' et les intermédiaires de calcul
#'
#' @format 32 lignes et 12 colonnes
#' \itemize{
#' \item age : age
#' \item sexe : sexe
#' \item px_sl : prévalence des incapacités lourdes
#' \item Lx_free_sl : nombres d'années vécues par âge par les membres de la cohorte fictive permettant le calcul des
#'     espérances de vie sans incapacités lourdes
#' \item Tx_free_sl : nombre d'années totales à vivre par les membres de la cohorte fictive permettant le calcul des
#'     espérances de vie sans incapacités lourdes
#' \item ex_free_sl : espérances de vie sans incapacités lourdes
#' \item prop_free_sl : part de l'espérance de vie passée sans incapacités lourdes
#' \item px_l : prévalence des incapacités légères
#' \item Lx_free_l : nombres d'années vécues par âge par les membres de la cohorte fictive permettant le calcul des
#'     espérances de vie sans incapacités légères
#' \item Tx_free_l : nombre d'années totales à vivre par les membres de la cohorte fictive permettant le calcul des
#'     espérances de vie sans incapacités légères
#' \item ex_free_l : espérances de vie sans incapacités légères
#' \item prop_free_l : part de l'espérance de vie passée sans incapacités légères}
"evsi_l_sl"
