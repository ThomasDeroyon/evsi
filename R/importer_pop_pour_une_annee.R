
#' Importation des données de population pour une année dans le tableau des données de population sur longue
#' période
#'
#' La fonction importer_pop_pour_une_annee est une fonction élémentaire qu'utilise la fonction
#' importer_pop_tableau_complet. Les données de pyramide des âges par sexe sur longue période sont stockées
#' dans un classeur excel récupéré sur le site de l'Insee où chaque année correspond à un onglet.
#' La fonction importer_pop_pour_une_annee télécharge les données d'un onglet
#'
#' @param chemin_pop_complet adresse physique du classeur excel contenant les données de population à
#'         télécharger
#' @param feuille_pop_complet nom de l'onglet correspondant à l'année à télécharger
#' @param skip_pop_complet nombre de lignes à sauter dans le chargement de l'onglet à télécharger
#'
#' @return Un data.frame de 13 colonnes et aucun de lignes que d'âges différents (de 0 0 100 en général).
#'      Les colonnes correspondent à :
#'  \itemize{
#'  \item annee_naissance : l'année de naissance
#'  \item age : l'âge en années atteintes dans l'année. Egal à l'année - l'année de naissance
#'  \item pop_h : le nombre total d'hommes par âge
#'  \item pop_h_celib : nombre total d'hommes célibataires par âge
#'  \item pop_h_marie : nombre total d'hommes mariés par âge
#'  \item pop_h_veuf : nombre total d'hommes veufs par âgé
#'  \item pop_h_divorce : nombre total d'hommes divorcés par âge
#'  \item pop_f : nombre total de femmes par âge
#'  \item pop_f_celib : nombre total de femmes célibataires par âge
#'  \item pop_f_veuf : nombre total de femmes veuves par âge
#'  \item pop_f_divorce : nombre totla de femmes divorcées par âge
#'  \item annee : année sur laquelle portent les données
#'  }
#'
#'  @author Thomas Deroyon

importer_pop_pour_une_annee <- function(chemin_pop_complet,
                                        feuille_pop_complet,
                                        skip_pop_complet){

  temp <-
    readxl::read_excel(
      path = chemin_pop_complet,
      sheet = feuille_pop_complet,
      skip = skip_pop_complet)

  if (ncol(temp) == 5){

    names(temp) <- c('annee_naissance', 'age', 'pop_tot', 'pop_h', 'pop_f')

    temp$pop_h_celib   <- NA
    temp$pop_h_marie   <- NA
    temp$pop_h_veuf    <- NA
    temp$pop_h_divorce <- NA
    temp$pop_f_celib   <- NA
    temp$pop_f_marie   <- NA
    temp$pop_f_veuf    <- NA
    temp$pop_f_divorce <- NA

  } else {

    names(temp) <- c('annee_naissance', 'age',
                     'pop_tot',
                     'pop_h', 'pop_h_celib', 'pop_h_marie', 'pop_h_veuf', 'pop_h_divorce',
                     'pop_f', 'pop_f_celib', 'pop_f_marie', 'pop_f_veuf', 'pop_f_divorce')

  }

  temp <- subset(temp, !is.na(pop_tot) & !is.na(age) & pop_tot != 'Ensemble')
  temp$annee <- feuille_pop_complet

  temp$pop_h_celib[temp$pop_h_celib == '.'] <- NA
  temp$pop_h_marie[temp$pop_h_marie == '.'] <- NA
  temp$pop_h_veuf[temp$pop_h_veuf == '.'] <- NA
  temp$pop_h_divorce[temp$pop_h_divorce == '.'] <- NA

  temp$pop_f_celib[temp$pop_f_celib == '.'] <- NA
  temp$pop_f_marie[temp$pop_f_marie == '.'] <- NA
  temp$pop_f_veuf[temp$pop_f_veuf == '.'] <- NA
  temp$pop_f_divorce[temp$pop_f_divorce == '.'] <- NA

  temp$age <- str_remove(temp$age, pattern = ' ou plus')

  return(temp)

}



