
#' Agrégation des valeurs pour les âges supérieurs ou égaux à 100
#'
#' La fonction agreger_99p part d'une variable contenant des effectifs par âge et agrège les données disponibles
#' pour les âges supérieurs à 99 ans pour les résumer en une seule valeur correspondant à la modalité 100 ans
#' et plus
#'
#' @param age vecteur caractère contenant les valeurs de la variable d'âge. Le vecteur peut contenir une valeur Total correspondant
#'     au total de la population
#' @param variable vecteur numérique contenant les valeurs de la variable correspondant à des totaux par âge. Le vecteur variable
#'     est dans le même ordre que le vecteur age
#' @return un data.frame contenant deux colonnes. Une colonne, dont le nom est identique à la valeur du paramètre variable, et une
#'     colonne age. La colonne age prend des valeurs caractère inférieures ou égales à '100'. Dans les lignes pour lesquelles age
#'     est inférieur ou égal à 99, l'autre colonne contient la valeur de la variable correspondant à l'âge en question. Pour la
#'     ligne où age est égale à 100, l'autre colonne contient la somme des valeurs de la variable pour les âges supérieurs ou égaux
#'     à 100. Il n'y a aucune ligne correspondant au total dans la population indépendamment de l'âge
#' @examples
#'     x <- rnorm(110)
#'     age <- as.character(1:110)
#'     x_agrege <- agreger_99p(variable = x, age = age)
#'
#' @author Thomas Deroyon

agreger_99p <- function(age, variable){

  age_99p <- age
  age_99p[nchar(age) >= 3 & substr(age, 1, 1) == '1'] <- '100'
  var_99p <- aggregate(variable,
                       by = list(age = age_99p),
                       FUN = sum)

  return(var_99p[var_99p$age != 'Total',])

}

